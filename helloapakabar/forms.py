from django import forms
from .models import HelloApaKabar


class HelloApaKabarForm(forms.ModelForm):
    class Meta:
        model = HelloApaKabar
        fields = ['status']
        widgets = {
            'status':forms.TextInput(attrs={'class':'form-control'}),
            }

